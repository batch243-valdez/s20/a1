let enterNumber = parseInt(prompt("Enter a number"));

console.log("The number you provided is " +enterNumber);
for (enterNumber; enterNumber >= 0; enterNumber-=5){
	if(enterNumber % 10 == 0){
	 	console.log("The number is divisible by 10. Skipping the number.")
	 	continue;
	}
	if(enterNumber <= 50){
		console.log("The current value is " + enterNumber + "terminating the loop");
	 	break;
	}
	if(enterNumber % 5 == 0){
		console.log(enterNumber);
	}
}

/*Create a variable that will contain the string supercalifragilisticexpialidocious.
Create another variable that will store the consonants from the string.
Create another for Loop that will iterate through the individual letters of the string based on it’s length.
Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.
Create an else statement that will add the letter to the second variable.
Create a git repository named S16.
Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
Add the link in Boodle.*/

let word = "supercalifragilisticexpialidocious.";
let	consonants = "";

for(let i=0; i<word.length;i++){
    if (word[i].toLowerCase()==='a'||
    	word[i].toLowerCase()==='e'||
    	word[i].toLowerCase()==='i'||
    	word[i].toLowerCase()==='o'||
    	word[i].toLowerCase()==='u'){
        continue;
    }
    else{
        consonants += word[i];
    }

}
console.log(word);
console.log("These are the consonants "+consonants);

